# frozen_string_literal: true

require 'rspec'
require '../players'

describe Player do
  it 'player name' do
    player = Player.new('John', 'X')
    expect(player.name).to eq('John')
  end
  it 'sign' do
    player = Player.new('John', 'X')
    expect(player.sign).to eq('X')
  end
  it 'sign' do
    player = Player.new('', 'X')
    expect(player.name).to eq('Player X')
  end
end
