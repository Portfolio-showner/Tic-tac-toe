# frozen_string_literal: true

# Macros messages
def welcome
  puts "Hi welcome to Showner's Game(basically its a Tic Tac Toe)\n\
  It should have two players\n"
  puts "You can exit the game anytime by writing 'exit'\n"
end

def false_statement(msg)
  puts msg
  false
end

def die(msg)
  puts msg
  exit
end
