# frozen_string_literal: true

# Class to display and store the game board
class Board
  attr_accessor :board
  def initialize
    @board = {}
    %w[1 2 3 4 5 6 7 8 9].each { |e| @board[e] = '' }
  end

  def line_chars(chars)
    left = chars[0].empty? ? ' ' : chars[0]
    middle = chars[1].empty? ? ' ' : chars[1]
    right = chars[2].empty? ? ' ' : chars[2]
    line(left, middle, right)
  end

  def line(left, middle, right)
    print left + '|' + middle + '|' + right + "\n"
  end

  def interline
    print "-----\n"
  end

  def display_where_to_play
    puts 'This is where you can play!'
    print line('7', '8', '9')
    print interline
    print line('4', '5', '6')
    print interline
    print line('1', '2', '3')
  end

  def display
    display_where_to_play
    puts 'Your turn'
    print line_chars(@board.values_at('7', '8', '9'))
    print interline
    print line_chars(@board.values_at('4', '5', '6'))
    print interline
    print line_chars(@board.values_at('1', '2', '3'))
  end
end
