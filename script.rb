# frozen_string_literal: true

require_relative 'macros_messages.rb'
require_relative 'players.rb'
require_relative 'board.rb'
require_relative 'inputs.rb'
require_relative 'check.rb'

# Class to launch the game
class Game
  def initialize
    welcome
    @game = Board.new
    @players = call_for_players
    @turns = 0
    @game.display
    @combi_win = [%w[1 2 3], %w[4 5 6], %w[7 8 9], %w[1 4 7], %w[2 5 8],
                  %w[3 6 9], %w[1 5 9], %w[3 5 7]]
  end

  def call_for_players
    puts 'Who will play the X sign?(Your name)'
    player1 = Player.new(gets.chomp, 'X')
    puts 'And for the O sign?(Your name too)'
    player2 = Player.new(gets.chomp, 'O')
    players = []
    players << player1
    players << player2
    players
  end

  def run
    turn(@players.first)
    @game.display
    checks
    turn(@players.last)
    @game.display
    checks
    true
  end

  def all
    run while run
  end
end

Game.new.all
