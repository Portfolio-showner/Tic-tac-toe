# frozen_string_literal: true

# Checks after every play
def checks
  win_check(@combi_win)
  turns_count_check
end

def turns_count_check
  @turns += 1
  die('Its a draw') if @turns == 9
end

def win_check(combi_win)
  combi_win.each do |e|
    @winner = 'X' if (e - @game.board.select { |_k, v| v == 'X' }.keys).empty?
    @winner = 'O' if (e - @game.board.select { |_k, v| v == 'O' }.keys).empty?
  end
  the_winner(@winner) if @winner
end

def the_winner(sign)
  player_name = @players.select { |player| player.sign == sign }.first.name
  die("#{player_name} Won")
end
