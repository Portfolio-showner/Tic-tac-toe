# frozen_string_literal: true

# Class to define players
class Player
  attr_accessor :name, :sign
  def initialize(name, sign)
    @name = if name.empty?
              'Player ' + sign
            else
              name
            end
    @sign = sign
  end
end
