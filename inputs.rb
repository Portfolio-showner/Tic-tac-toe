# frozen_string_literal: true

# Input and playing status
def turn(player)
  player_prompt(player)
  playing(player)
end

def player_prompt(player)
  puts "Where do you want to play #{player.name}?"
  @position = gets.chomp
  die('Ok, you will quit the game') if @position == 'exit'
end

def playing(player)
  if (1..9).cover?(@position.to_i)
    if @game.board[@position].empty?
      (@game.board[@position] = player.sign)
      return true
    else false_statement("you can't play here")
    end
  else
    false_statement('sorry but you can only choose from 1 to 9')
  end
  playing(player) unless self
end
