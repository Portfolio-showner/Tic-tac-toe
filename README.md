# Welcome to the de Tic Tac Toe game of [Showner](https://gitlab.com/rnshow)!

### Before trying it:
1. You should have ruby version 2.5.1

### To play you'll have to :
1. Clone the repository
2. Run 'bundle install'
2. Execute script.rb with 'ruby script.rb'
3. Follow instructions


### Tests
Tests are pending

Created with :heart: in France by [Showner](https://gitlab.com/rnshow)
